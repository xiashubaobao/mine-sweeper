# app.py
from flask import Flask, render_template

app = Flask(__name__)

DIFFICULTY_SETTINGS = {
    'beginner': {'rows': 9, 'cols': 9, 'mines': 10},
    'intermediate': {'rows': 16, 'cols': 16, 'mines': 40},
    'expert': {'rows': 16, 'cols': 30, 'mines': 99}
}

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/<difficulty>')
def game(difficulty):
    if difficulty not in DIFFICULTY_SETTINGS:
        difficulty = 'beginner'
    settings = DIFFICULTY_SETTINGS[difficulty]
    return render_template('game.html', 
                         rows=settings['rows'], 
                         cols=settings['cols'], 
                         mines=settings['mines'],
                         difficulty=difficulty)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)

